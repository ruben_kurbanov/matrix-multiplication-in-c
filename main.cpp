#include <iostream>
#include "MainPanel.h"

int main (int argc, char* argv[]){


    std::string command;
    do{
        std::cout << "Enter the command (enter --help for help):";
        std::cin >> command;
        Main m = Main();

        try{
            if(command == "-cmdsing"){
                std::cout << "Start!" << std::endl;
                m.startProgram();

            }else if(command == "-rand"){
                m.startProgramForRand();

            }else if(command == "-file"){
                m.startProgramForFile();

            }else if(command == "-cmdmult"){
                m.startProgramForMultithreading();

            }else if(command == "--help"){
                std::cout << "-cmdsing - Matrix multiplication via command line "
                             "(single-threaded). Input example:" << std::endl
                             << "Enter the dimensions of the first matrix:2 2" << std::endl << "3 3" << std::endl << "1 2"
                             << std::endl << "The sign * is automatically displayed." << std::endl
                             << "Enter the dimensions of the second matrix:2 1" << std::endl
                             << "1" << std::endl << "2" << std::endl << "Your calculated matrix:" << std::endl
                             << "2 1" << std::endl << "9" << std::endl << "5" << std::endl << std::endl;
                std::cout << "-cmdmult - Matrix multiplication via command line "
                             "(multithreaded). Input example:" << std::endl
                          << "Enter the dimensions of the first matrix:2 2" << std::endl << "3 3" << std::endl << "1 2"
                          << std::endl << "The sign * is automatically displayed." << std::endl
                          << "Enter the dimensions of the second matrix:2 1" << std::endl
                          << "1" << std::endl << "2" << std::endl << "Your calculated matrix:" << std::endl
                          << "2 1" << std::endl << "9" << std::endl << "5" << std::endl << std::endl;
                std::cout << "-file - Data entry through a text file. Example of file content: " << std::endl
                        << "2 2" << std::endl << "3 3" << std::endl << "1 2" << std::endl << std::endl
                        << "2 1" << std::endl << "1" << std::endl << "2" << std::endl << std::endl;

                std::cout << "-rand - Generation of random matrices for given sizes and their multiplication."
                            << std::endl << std::endl;

                std::cout << "To exit the program, enter -exit." << std::endl;

            }else if(command != "-exit"){
                std::cout << "Unknown team: " + command ;
            }
        } catch (int e) {
            std::cout << "Error!";
        }

        std::cout << std::endl;

    }while (command != "-exit");

    return 0;

}
