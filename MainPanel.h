#ifndef SEMESTRALKA_MAINPANEL_H
#define SEMESTRALKA_MAINPANEL_H


class Main{

private:
    struct Matice{
        int pocetRadk = 0;
        int pocetSloup = 0;
        int **array = nullptr;
    };

    Matice prvniMatice;
    Matice druhaMatice;
    Matice exitMatice;

    const char sign = '*';

public:
    Main();
    ~Main();

    /**
     *Checks if the dimensions of the entered matrices are correct.
     */
    void controlIfSizeOfMatrixIsTrue() const;

    /**
     *
     * @param radk
     * @param sloup
     * @param arrInt
     *
     * Checks if the number of elements in the matrices matches their sizes.
     */
    void controlSizeOfMatrix(int radk, int sloup, int arrInt);

    /**
     *
     * @param m
     *
     * Enters data as a string, iterates over each element to validate the entered data.
     * Creates an array and writes the entered elements into it.
     */
    void inputData(Matice &m);

    /**
     *
     * @param m1
     * @param m2
     *
     * Creates a calculated matrix (two matrices m1,m2 are multiplied).
     */
    void multyplMatrix(Matice m1, Matice m2);

    /**
     *
     * @param m
     *
     * Frees the memory of a two-dimensional dynamic array m.
     */
    void freeMemory(Matice &m);

    /**
     *
     * @param m
     *
     *Writes out the matrix to the command line.
     */
    void printMatrix(Matice m, std::string s);

    /**
     *Method for -rand. Creates two random arrays by the entered sizes and multiplies them.
     */
    void startProgramForRand();

    /**
     *Method for -cmdsing (for single-threaded matrix multiplication).
     */
    void startProgram();

    /**
     *Method for -file. Requests the address of a text document with matrices and multiplies them.
     */
    void startProgramForFile();

    /**
     *Method for -cmdmult (for multithreaded matrix multiplication).
     */
    void startProgramForMultithreading();

};

#endif //SEMESTRALKA_MAINPANEL_H
