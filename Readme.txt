Téma: „Násobení matic“.
Zadání:

Násobení matic je jedna z výpočetně nejzajímavějších operací v lineární algebře. Zaprvé protože je velmi časté, zadruhé protože je to operace, která dobře naimplementovaná opravdu může dosáhnout limitu výpočetní rychlosti procesoru (narozdíl třeba od sčítání vektorů, které jsou omezeny rychlostí přístupu do paměti).

Očekáváme, že naimplementujete něco sofistikovanějšího než 3 for loopy dle definice maticového násobění.


//------------------------Popís implementace:---------------------------------
Můj program je iterativní.
V hlavičkovém souboru(main.cpp) je vyžadován konkrétní příkaz, v závislosti na metodě zadávání dat (soubor, příkazový řádek), volbě počtu vláken. Můj program je také schopen vytvořit náhodná pole a znásobit je stejným způsobem. Po zadání příkazu se ze souboru MainPanel.h vytvoří instance třídy Main, do které se zadávají data, zkontroluje se vstupní formát, velikost pole, tvorba matic pomocí struktury Matice, jejich násobení a výstup. K vytvoření náhodných matic je vytvořena instance třídy Random ze souboru RandomMatrix.cpp, ve kterém nastavíme velikosti matice a intervaly náhodných čísel.



//-----------------------------Spuštění:---------------------------------------

-cmdsing - Matrix multiplication via command line (single-threaded). Input exa
mple:
Enter the dimensions of the first matrix:2 2
3 3
1 2
The sign * is automatically displayed.
Enter the dimensions of the second matrix:2 1
1
2
Your calculated matrix:
2 1
9
5

-cmdmult - Matrix multiplication via command line (multithreaded). Input example:
Enter the dimensions of the first matrix:2 2
3 3
1 2
The sign * is automatically displayed.
Enter the dimensions of the second matrix:2 1
1
2
Your calculated matrix:
2 1
9
5

-file - Data entry through a text file. Example of file content:
2 2
3 3
1 2

2 1
1
2

-rand - Generation of random matrices for given sizes and their multiplication.

To exit the program, enter -exit.


//-------------------------------Vstup přes file------------------------------
Chcete-li otestovat zadávání dat prostřednictvím souboru, je k dispozici - test.txt.

Příklad:

Enter the command (enter --help for help):-file
Enter the address of the .txt file (For example: C:CLionProjectsmatrix.txt) -
C:\Users\Ruben\CLionProjects\semestralka\test.txt
First matrix:
10 10
287 98 108 242 234 39 77 282 131 146
45 263 175 176 159 98 25 31 20 46
177 288 92 168 44 237 281 266 219 92
21 244 244 69 137 248 186 24 266 244
87 263 265 169 85 65 202 230 83 21
287 29 207 112 195 112 176 219 10 188
19 88 289 250 123 217 149 32 124 173
280 142 26 148 183 213 213 12 20 22
208 180 31 273 88 291 250 58 18 206
217 235 157 245 176 40 198 59 163 184
*
Second matrix:
10 1
1
0
1
0
0
1
1
0
1
0
Your calculated matrix:
10 1
642
363
1006
965
702
792
798
752
798
775


//-------------------------------Vstup přes přikazkový řádek------------------
Chcete-li otestovat zadávání dat prostřednictvím konzoly, je k dispozici - examples.txt se čtyřmi příklady a správnými odpověďmi pro ověření.

Příklad:

Enter the command (enter --help for help):-cmdsing
Start!
1 3
-59 78 -85;
*
3 1
78
-28
-97;
Your calculated matrix:
1 1
1459


//-------------------------------Doba spracování vlaken----------------------------- 
Doba potřebná k spočítání příkladu ze souboru test.txt: - pro jedno vlákno - 6561865 ms
                                                        - pro vícevláknové - 6433678 ms

//----------------------------------Poznámka----------------------------------------

1)Když dělate vstup přes přikazkový řádek po vložení prvků matice nezapoměnte na konci dodat ";".
2)Při použivání vícevláknové počitání matic data v první matice se rozdělí na sudé a liché řadky a budou se spočitávat v růyných vlaknách.
3)Pro vstup přes file povolený format jenom .txt .
