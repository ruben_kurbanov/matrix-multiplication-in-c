#include <string>
#include <vector>
#include "RandomMatrix.cpp"
#include "MainPanel.h"
#include <fstream>
#include <iostream>
#include <thread>

template <typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

Main::Main() {}

Main::~Main() {
    if(prvniMatice.array) freeMemory(prvniMatice);
    if(druhaMatice.array) freeMemory(druhaMatice);
    if(exitMatice.array) freeMemory(exitMatice);
}

void Main::controlIfSizeOfMatrixIsTrue() const {
    if(prvniMatice.pocetSloup != druhaMatice.pocetRadk)throw 3;
}

void Main::controlSizeOfMatrix(int radk, int sloup, int arrInt){
    if(radk*sloup > arrInt || radk*sloup < arrInt) throw 1;
}


void createMatrix(int **&array, int rad, int sloup){
    array = new int* [rad];
    for (int i = 0; i < rad; ++i) {
        array[i] = new int [sloup];
    }
}

void putDataInMatric(int **&array, int rad, int sloup, std::vector<int> vec){
    int q = 0;
    for (int i = 0; i < rad; ++i) {
        for (int j = 0; j < sloup; ++j) {
            array[i][j] = vec[q];
            q++;
        }
    }
}

void Main::inputData(Matice &m){

    std::cin >> m.pocetRadk >> m.pocetSloup;

    std::vector<std::string> arr;
    std::string ar;
    std::vector<int> arrInt;
    std::string a = "";

    while (std::cin >> ar){
        std::string nomber;
        if(ar[ar.size()-1] == ';'){

            nomber = "";
            int i = 0;
            while(ar[i] != ';'){
                nomber += ar[i];
                i++;
            }
            if(!nomber.empty()) arr.push_back(nomber);

            break;
        }
        arr.push_back(ar);

    }

    controlSizeOfMatrix(m.pocetRadk, m.pocetSloup, arr.size());

    for (int i = 0; i < arr.size(); ++i) {

        for (int j = 0; j < arr[i].size(); ++j) {
            if (!(bool)isdigit(arr[i][j]) && !((arr[i][j] == '-' && j == 0)
            && j + 1 < arr[i].size() && isdigit(arr[i][j + 1]))) throw 2;
        }
        arrInt.push_back(stoi(arr[i]));

    }

    std::cin.clear();

    createMatrix(m.array, m.pocetRadk, m.pocetSloup);
    putDataInMatric(m.array, m.pocetRadk, m.pocetSloup, arrInt);

}

void Main::multyplMatrix(Matice m1, Matice m2){
    int element = 0;

    for(int i = 0; i < m1.pocetRadk; i++){
        for(int j = 0; j < m2.pocetSloup; j++){
            element = 0;
            for(int k = 0; k < m1.pocetSloup; k++){
                element += m1.array[i][k] * m2.array[k][j];
            }
            exitMatice.array[i][j] = element;
        }
    }
}

void Main::freeMemory(Matice &m) {
     for (int i = 0; i < m.pocetRadk; i++) {
         delete [] m.array[i];
     }
     delete [] m.array;
     m.pocetRadk = 0;
     m.pocetSloup = 0;
}


void Main::printMatrix(Matice m, std::string s){
    std::cout << s << std::endl;
    std::cout << m.pocetRadk << " " << m.pocetSloup << std::endl;

    for (int i = 0; i < m.pocetRadk; ++i) {
        for (int j = 0; j < m.pocetSloup; ++j) {
            if(j == 0){
                std::cout << m.array[i][j];
            }else{
                std::cout << " " << m.array[i][j];
            }
        }
        std::cout << std::endl;
    }
}

void Main::startProgramForRand(){
     Random rand = Random();
     std::string agree = "";
     do{
         if(prvniMatice.array) freeMemory(prvniMatice);

         rand.createMatrix(prvniMatice.array, 1);
         prvniMatice.pocetRadk = rand.getCountOfRowsFirstMatrix();
         prvniMatice.pocetSloup = rand.getCountOfLinesFirstMatrix();
         printMatrix(prvniMatice, "First matrix:");

         std::cout << "Are you agree with this Matrix?(Enter ok, if YES or something else, if NO)" << std::endl;
         std::cin >> agree;
         if (agree == "ok") break;
     }while (true);

     agree = "";
     do{
         if(druhaMatice.array) freeMemory(druhaMatice);

         rand.createMatrix(druhaMatice.array, 2);
         druhaMatice.pocetRadk = rand.getCountOfRowsSecondMatrix();
         druhaMatice.pocetSloup = rand.getCountOfLinesSecondMatrix();
         printMatrix(druhaMatice, "Second matrix:");

         std::cout << "Are you agree with this Matrix?(Enter ok, if YES or something else, if NO)" << std::endl;
         std::cin >> agree;
         if (agree == "ok") break;
     }while (true);

     exitMatice.pocetRadk = prvniMatice.pocetRadk;
     exitMatice.pocetSloup = druhaMatice.pocetSloup;
     createMatrix(exitMatice.array, exitMatice.pocetRadk, exitMatice.pocetSloup);
     multyplMatrix(prvniMatice, druhaMatice);
     printMatrix(exitMatice, "Your calculated matrix:");
}

void Main::startProgram() {

    inputData(prvniMatice);
    std::cout << sign << std::endl;
    inputData(druhaMatice);

    controlIfSizeOfMatrixIsTrue();

    exitMatice.pocetRadk = prvniMatice.pocetRadk;
    exitMatice.pocetSloup = druhaMatice.pocetSloup;
    createMatrix(exitMatice.array, exitMatice.pocetRadk, exitMatice.pocetSloup);
    multyplMatrix(prvniMatice, druhaMatice);

    printMatrix(exitMatice, "Your calculated matrix:");

}

void Main::startProgramForFile(){

    std::fstream F;
    std::string f;
    std::vector<int> vec;

    std::cout << "Enter the address of the .txt file (For example: C:\CLionProjects\matrix.txt) - ";
    std::cin >> f;

    F.open(f);

    if (!F){
        F.close();
        throw 4;
    }

    F >> prvniMatice.pocetRadk;
    F >> prvniMatice.pocetSloup;

    for (int i = 0; i < prvniMatice.pocetRadk*prvniMatice.pocetSloup; ++i) {vec.push_back(0);
        if (!(F >> vec[i])){
            F.close();
            throw 2;
        }
    }

    createMatrix(prvniMatice.array, prvniMatice.pocetRadk, prvniMatice.pocetSloup);
    putDataInMatric(prvniMatice.array, prvniMatice.pocetRadk, prvniMatice.pocetSloup, vec);
    vec.clear();

    F >> druhaMatice.pocetRadk;
    F >> druhaMatice.pocetSloup;

    for (int i = 0; i < druhaMatice.pocetRadk*druhaMatice.pocetSloup; ++i) {
        vec.push_back(0);
        if (!(F >> vec[i])){
            F.close();
            throw 2;
        }
    }


    createMatrix(druhaMatice.array, druhaMatice.pocetRadk, druhaMatice.pocetSloup);
    putDataInMatric(druhaMatice.array, druhaMatice.pocetRadk, druhaMatice.pocetSloup, vec);
    vec.clear();
    F.close();

    printMatrix(prvniMatice, "First matrix:");
    std::cout << sign << std::endl;
    printMatrix(druhaMatice, "Second matrix:");

    controlIfSizeOfMatrixIsTrue();
    exitMatice.pocetRadk = prvniMatice.pocetRadk;
    exitMatice.pocetSloup = druhaMatice.pocetSloup;
    createMatrix(exitMatice.array, exitMatice.pocetRadk, exitMatice.pocetSloup);
    multyplMatrix(prvniMatice, druhaMatice);
    printMatrix(exitMatice, "Your calculated matrix:");

}

void run(std::vector<int> v1, std::vector<int> v2 , int &r) {
    r = 0;
    for (int i = 0; i < v1.size(); ++i) {
        r = r + v1[i]*v2[i];
    }
}

void Main::startProgramForMultithreading(){
    inputData(prvniMatice);
    std::cout << sign << std::endl;
    inputData(druhaMatice);
    controlIfSizeOfMatrixIsTrue();

    exitMatice.pocetRadk = prvniMatice.pocetRadk;
    exitMatice.pocetSloup = druhaMatice.pocetSloup;
    createMatrix(exitMatice.array, exitMatice.pocetRadk, exitMatice.pocetSloup);

    std::vector<std::vector<int>> vec;
    std::vector<std::vector<int>> vec2;
    std::vector<int> v;

    for (int i = 0; i < prvniMatice.pocetSloup; ++i) {
        vec.push_back(v);
        for (int j = 0; j < prvniMatice.pocetRadk; ++j) {
            vec[i].push_back(prvniMatice.array[i][j]);
        }
    }

    for (int i = 0; i < druhaMatice.pocetSloup; ++i) {
        vec2.push_back(v);
        for (int j = 0; j < druhaMatice.pocetRadk; ++j) {
            vec2[i].push_back(druhaMatice.array[j][i]);
        }
    }

    for (int i = 0; i < vec2.size(); ++i) {
        for (int j = 0; j < vec.size(); ++j) {
            if (j%2 == 1 || j == 0){
                std::thread t1(run, vec2[i], vec[j], std::ref(exitMatice.array[j][i]));
                t1.join();
            }else{
                std::thread t2(run, vec2[i], vec[j], std::ref(exitMatice.array[j][i]));
                t2.join();
            }

        }
    }

    printMatrix(exitMatice, "Your calculated matrix:");
}

