#include <iostream>
#include <ctime>
#include <random>

class Random{

private:
    int r1, s1, r2, s2;
    int interval[2];

public:

    /**
     *
     * @param min
     * @param max
     * @return
     *
     * Random number generator.
     * A source: https://cw.fel.cvut.cz/wiki/courses/b6b36pjc/ukoly/semestralka
     */
    double get_random_double(int min, int max) {

        std::mt19937 mt{ std::random_device{}() };
        std::uniform_real_distribution<> dist(min, max);
        return dist(mt);
    }

    /**
     *
     * @param array
     * @param w
     *
     * Creates an array of random numbers to the address array.
     */
    void createMatrix(int **&array, int w){
        if(w == 2){
            std::cout << "Number of rows of the second matrix (=): " << s1 << std::endl;
            std::cout << "Enter the number of lines(||): " << std::endl;
            std::cin >> s2;
            std::cout << std::endl;

        }else{
            std::cout << "Enter the number of rows(=):" << std::endl;
            std::cin >> r1;
            std::cout << std::endl << "Enter the number of lines(||):" << std::endl;
            std::cin >> s1;
            std::cout << std::endl;
            r2 = s1;

        }
        int Rad;
        (w == 2) ? Rad = r2: Rad = r1;
        int Slo;
        (w == 2) ? Slo =  s2: Slo = s1;

        std::cout << "Enter a range of numbers (e.g. -10 10"
                     "   -interval from -10 to 10 inclusive)" << std::endl;
        std::cin >> interval[0] >> interval[1];
        std::cout << std::endl;

        array = new int* [Rad];
        for (int i = 0; i < Rad; ++i) {
            array[i] = new int [Slo];
        }

        for (int i = 0; i < Rad; ++i) {
            for (int j = 0; j < Slo; ++j) {

                array[i][j] = (int)get_random_double(interval[0], interval[1] + 1);

            }
        }
        interval[0] = 0;
        interval[1] = 0;

    }

    int getCountOfRowsFirstMatrix() const{return r1;}
    int getCountOfLinesFirstMatrix() const{return s1;}
    int getCountOfRowsSecondMatrix() const{return r2;}
    int getCountOfLinesSecondMatrix() const{return s2;}

};

